import React from 'react';
import { useRef, useState, useEffect } from 'react';

import './Type10.scss';

const Type10 = () => {
    let [numArray, setNumArray] = useState([]); // <== REFERENCE changes to this force the component to re-render.
    let numberRef = useRef(0); // <== Changes to this NEVER force the component to re-render.

    useEffect(() => {
        console.log('A reference to numArray has changed!');
    }, [numArray]); // <== Dependency array: useEffect() ONLY fires when a REFERENCE change to numArray has occurred.

    const onHandleClick = () => {
        ++numberRef.current;
        numArray.push(numberRef.current);
        setNumArray(numArray); // <== Does not force the component to re-render.
        //setNumArray([...numArray]); // <== This is a REFERENCE change and forces the component to re-render and as a side effect, the updated numberRef.current is also displayed.

        // Adding numbers to an array is not a change that useEffect() can detect.
        // UseEffect() needs a reference change, in other words, a newly created array that points
        // to a new space in memory fires useEffect(). This same behavior applies to  any state 
        // variable that is an object (dictionary, set, your class, etc.).
        // There is no React API that can force a React Component to re-render.
    }

    return (
        <div className='Type10'>
            <button onClick={() => onHandleClick()}>Push</button>
            <span>{numberRef.current}</span>
            <span>{numArray.join()}</span>
        </div>
    )
}

export default Type10;