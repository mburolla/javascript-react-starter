import React from 'react';
import { Child } from './Child'
import { useState } from 'react';

import './Type9.scss';

const Type9 = () => {
    let [number, setNumber] = useState(0);

    const onHandleNumber = (n) => {
        setNumber(n);
    }

    return (
        <div className="Type9">
            <Child number="3" onHandleNumber={(n) => onHandleNumber(n)}/>
            <span>{number}</span>
        </div>
    )
}

export default Type9;