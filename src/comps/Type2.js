import React from 'react';

import './Type2.scss';

const Type2 = (props) => { // Optionally, destructure props: { message }
  return (
    <div className="Type2">
        {props.message}
        {/* { message } */}
    </div>
  )
}

export default Type2;
