import React from 'react';
import { useState } from 'react';

import './Type11.scss';

const Type11 = () => {
    let [show, setShow] = useState(false);
    let [enableInput, setEnableInput] = useState(true);

    return (
        <div className="Type11">
            <button onClick={() => setShow(!show)}>Push</button>
            {
                show && // <== Add/remove the div from the DOM.
                <div>
                { /* <div style={show ? {} : {display:'none'}}> */ }
                    <button onClick={()=> setEnableInput(!enableInput)}>Push</button>
                    <input disabled={enableInput}></input>
                </div>
            }
        </div>
    )
}

export default Type11;

//
// show && <== Add/remove the div from the DOM.
// <div style={show ? {} : {display:'none'}}> <== Show/hide the div from the user, but keep it in the DOM.
// 
