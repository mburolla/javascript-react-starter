import React from 'react';
import { useState } from 'react';

import './Type4.scss';

const Type4 = () => {
    let [counter, setCounter] = useState(0);

    const onClick = () => {
        setCounter(++counter); // <== Note, increment the value of counter, THEN use it.
    }

    return (
        <div className="Type4">
            <button onClick={() => { onClick() }}>Push</button>
            <span>{counter}</span>
        </div>
    )
}

export default Type4;

// 
// <button onClick={() => { onClick() }}>Push</button>
//
// () => {  } ==> Is a function
// () => { onClick() } ==> Is a function that calls another function: onClick().
//