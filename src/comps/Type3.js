import React from 'react';

import './Type3.scss';

const Type3 = (props) => {
  return (
    <div className="Type3">
        {props.children}
    </div>
  )
}

export default Type3;