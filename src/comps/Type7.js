import React from 'react';
import { useState, useEffect } from 'react';
import axios from 'axios'; // <== Note: No curly braces here.

import './Type7.scss';

const Type7 = () => {
    let [firstName, setFirstName] = useState("");

    useEffect(() => {
        const fetch = async () => {
            let firstName = (await axios.get(`https://jsonplaceholder.typicode.com/users/1`)).data.name;
            setFirstName(firstName);
        }
        fetch();
    }, []); // <== NOTE: Empty bracket forces this useEffect() to fire ONLY ONCE when the component loads.

    //
    // IMPORTANT!!!! 
    // Removing the empty brackets [] (AKA dependency array) from useEffect causes an infinite loop!
    // This is because changing the state forces the component to re-render, which forces 
    // useEffect() to fire, which changes the state, which forces a re-render and so on...
    //

    //
    // IMPORTANT!!!! 
    // ALWAYS keep the Network tab open in the DevTools and select Fetch/XHR to make sure you are
    // not recursively calling the API!  XHR stands for XmlHttpRequest which is a library
    // that ships with the browser which makes async API calls.  It was built for XML SOAP 
    // requests, but it works fine for JSON requests.
    //

    return (
        <div className='Type7'>
            {firstName}
        </div>
    )
}

export default Type7;