# JavaScript React Starter
Reference repo that describes React and React Components.  

# Getting Started
- Clone this repo
- Install dependencies: `npm install`
- Start the app: `npm start`

# Videos
- [React: Getting Started - The Basics - Modern JavaScript (1hr 30m)](https://app.pluralsight.com/library/courses/react-js-getting-started/table-of-contents)
- [A Practical Start with React (2h)](https://app.pluralsight.com/library/courses/react-practical-start/table-of-contents)
- [JSX (35m)](https://app.pluralsight.com/course-player?clipId=eeb59617-32bc-4e8c-a1c4-e10589d19a4d)
- [ReactJS Basics - #9 How does ReactJS update the DOM? (5m)](https://www.youtube.com/watch?v=Iw2BLUjQo1E)
  - Dev Tool >> More Tools >> Rendering >> Paint Flashing
- [Understanding React's UI Rendering Process (30min)](https://youtu.be/i793Qm6kv3U)

# Links
- [UseEffect for Beginners](https://www.freecodecamp.org/news/react-useeffect-absolute-beginners/)
- [Single Responsibility Principle](https://en.wikipedia.org/wiki/Single-responsibility_principle) (SRP)

# Contents
- [Overview](overview.md)
- [Create New React Application](create-new.md)
- [Components](/components.md)
- [Component Types](/component-types.md) *** Brilliant ***

# Important Notes
- ALWAYS keep the console window open when developing React applications!
- Address the issues in the console IMMEDIATELY!
- ALWAYS look at the console when developing your React application!
- When making API calls ALWAYS look at the console (Network tab - XHR) to make sure you are not recursively calling APIs!
- There is a zen to React development... sometimes the live server gets confused and needs to be
restarted
- ProTips: 
  - Dev Tools >> More Tools >> Rendering >> Paint flashing
  - Dev Tools >> More Tools >> Rendering >> Performance monitor

