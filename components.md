# React Components
Components come in [two flavors](https://www.telerik.com/blogs/react-class-component-vs-functional-component-how-choose-whats-difference?kw=&cpn=15387578594&&utm_source=google&utm_medium=cpc&utm_campaign=kendo-ui-react-trial-search-bms-NA&ad_group=DSA+Ad+Group&utm_term=DYNAMIC+SEARCH+ADS&ad_copy=&ad_type=DSA&ad_size=&ad_placement=&gclid=CjwKCAiA866PBhAYEiwANkIneCBWh5f3YivZTiaPEWRCX4MKd7A_BoHUtf8eojVslE5tlJH_hVHo-xoCkCQQAvD_BwE&gclsrc=aw.ds): Function or Class based.

Facebook has 50K React Components in production.

#### Function Components (Modern approach)

```
import { useState } from 'react';
const [personId, setPersonId] = useState(true);

function Welcome(props) {
    return (
        <h1>Hello, {props.name}</h1>
    );
}
``` 

#### Class Components (Legacy approach)

```
class Welcome extends React.Component {
    constructor(props) {
        super(props);
        this.personId = 0;
    }

    render() {
        return (
            <h1>Hello, {props.name}</h1>
        );
    }
}
``` 

- React Components MUST begin with a capital letter
- Hooks cannot be used in Class Components, but can be used in Function Components
- Components should be small and focused, favoring the [Single Responsibility Principle](https://en.wikipedia.org/wiki/Single-responsibility_principle) (SRP)

# Props 
  - As of React 16, Component props are READ ONLY
  - Props stay the same for the component's lifetime
  - Props are passed into a Component by its parent
  - If the Component props have not changed, then a Component is not re-rendered
  - Type Checking Validation: propTypes runtime validation of properties
    - `npm install prop-types`
    - `import PropTypes from 'prop-types'`
  - `getDerivedStateFromProps()` can be used to set state when a property is set
  - Values and callback functions are passed down from the parent to child 
  Component properties
  - "Prop drilling" referes to passing properties down from one Component to another, not best practice and can be avoided using the React context API

# State 
  - IMPORTANT: A state change occurs when the reference to the state variable changes (e.g. Adding items to an array or dictionary is not a "reference" change)
  - There is NO API that you can call to force a component to re-render.  If you find yourself asking, how can I 
  force this component to re-render, you are probably not creating a reference change and/or you need to change
  your approach to the component(s).
  - State can only be declared inside a React Component
  - State is local mutable data within the Component that can "survive" re-renders
  - `useState([])` is used in Functional Components whereas `this.setState()` is used in Class Components
  - Keep the state as close as possible to where it is actually being used
  - "Lifting state" refers to moving state variables up one or more parent levels
  - [YouTube](https://youtu.be/ipHlH4VE7Wo)
  - You should design Components that don't use state as much as possible (this are the easiest comps)

# Hooks
- Functional Components use hooks to add behavior
- Three most common hooks: `useState`, `useEffect`, `useRef`
- Always call hooks at the top level of your component. Do not conditionally call 
hooks or deeply nest them in your components (they won't work correctly).

|Hook|Description|Has Dependency Array|
|----|-----------|--------------------|
|useState|Variables used to store the state of a component.  Changes to state variables force a re-render ("reference changes").|No|
|useEffect|Do something when a reference to something listed in the dependency array has changed, or only do something when the component is mounted into the DOM the first time (Empty dependency array: `[]`).|Yes|
|useRef|Wrap something so that it is preserved during the whole component's lifetime, accessible by `.current`. Conceptually, useRef variables act like under-the-hood private class instance variables.  UseRef variables are also used to get a reference to HTML elements similar to `getElementById()`.  Changes to useRef variables do not force a re-render.|No|
|useMemo|Returns a cached value.  This caches something once or conditionally when something listed in the dependency array has changed.|Yes|
|useCallback|Returns a cached function.  This makes a function remain the same across re-renders, used to avoid an infinite loop (e.g API call, update state).|Yes|

# Passing Things into Components
- Boolean
- Array
- Dictionary
- Strings

