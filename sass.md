# SASS
SASS is short for Sassy CSS.

Three reasons to use Sass:
- Nested rules sets, no need to use large CSS selectors such as: `body > div > div > div`
- Ability to create variables
- Easy comments with "//", don't need to use /* */

# Video
[Crash Course](https://youtu.be/Zz6eOVaaelI)
